# Maintainer: tioguda <guda.flavio@gmail.com>

pkgbase=ddutility
pkgname=("${pkgbase}-bin" "${pkgbase}-bin-pt-br")
pkgver=1.6
pkgrel=1.7
pkgdesc="Write and Backup Operating System IMG and ISO files on Memory Card or Disk"
arch=('any')
license=('custom')
url="https://www.thefanclub.co.za/how-to/dd-utility-write-and-backup-operating-system-img-and-iso-files-memory-card-or-disk"
makedepends=('imagemagick')
provides=("${pkgbase}")
options=('!strip' '!emptydirs')
source=("https://raw.githubusercontent.com/thefanclub/dd-utility/master/DEB/${pkgbase}_${pkgver}_all.deb"
        https://metainfo.manjariando.com.br/${pkgbase}/com.${pkgbase}{,-pt-br}.metainfo.xml
        "https://metainfo.manjariando.com.br/${pkgbase}/com.${pkgbase}.desktop"
        "translation_pt-br.patch")
sha256sums=('bf01d8fc8fafaf76ab751fbcf6b54d75930581e62d602fcaf4cd28fe23579224'
            'a7b473da50be503f8abfb3ce1ffa4b0cfc2020674ffdd8529047e7c0c02e0144'
            '45403d46b20805a03bcc60de3dcfa1974b4e074276107a3850746dbe8fe88092'
            'b3cea8e4e00df0cccfacdcdd93e138c485b625c0ba2616368d91b7801a512e2f'
            'de5c1fca6693f3b545ea5094a921fe67a3bbc43372f67992fee9bb06a8e26083')

package_ddutility-bin(){
    depends=('bash' 'coreutils' 'gzip' 'lsof' 'udevil' 'xz' 'zenity' 'zip')
    conflicts=("${pkgbase}")

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    # Appstream
    rm -rf "${pkgdir}/usr/share/applications/${pkgbase}.desktop" 
    install -Dm644 "${srcdir}/com.${pkgbase}.desktop" "${pkgdir}/usr/share/applications/com.${pkgbase}.desktop"
    install -Dm644 "${pkgdir}/usr/share/doc/${pkgbase}/copyright" "${pkgdir}/usr/share/licenses/${pkgbase}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgbase}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgbase}.metainfo.xml"

    for size in 22 24 32 48 64 128 256 512; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/opt/thefanclub/${pkgbase}/${pkgbase}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgbase}.png"
    done
}

package_ddutility-bin-pt-br() {
    depends=('bash' 'coreutils' 'gzip' 'lsof' 'udevil' 'xz' 'zenity' 'zip')
    conflicts=("${pkgbase}")

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    cd "${pkgdir}"
    patch -p1 -i ${srcdir}/translation_pt-br.patch

    # Appstream
    rm -rf "${pkgdir}/usr/share/applications/ddutility.desktop" 
    install -Dm644 "${srcdir}/com.${pkgbase}.desktop" "${pkgdir}/usr/share/applications/com.${pkgbase}_pt_br.desktop"
    install -Dm644 "${pkgdir}/usr/share/doc/ddutility/copyright" "${pkgdir}/usr/share/licenses/${pkgbase}/LICENSE"
    install -Dm644 "${srcdir}/com.${pkgbase}-pt-br.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgbase}-pt-br.metainfo.xml"

    sed -i 's|Icon=ddutility|Icon=ddutility-pt-br|' "${pkgdir}/usr/share/applications/com.${pkgbase}_pt_br.desktop"

    for size in 22 24 32 48 64 128 256 512; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/opt/thefanclub/${pkgbase}/${pkgbase}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgbase}-pt-br.png"
    done
}
